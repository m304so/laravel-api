<?php

use Illuminate\Database\Seeder;
use App\Article;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Заполнить таблицу Article тестовыми данными
     *
     * @return void
     */
    public function run()
    {
        Article::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            Article::create([
                'name' => $faker->sentence,
                'body' => $faker->paragraph,
            ]);
        }
    }
}
